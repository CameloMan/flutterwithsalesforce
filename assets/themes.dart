import 'package:flutter/material.dart';
import 'utils.dart';

var mainTheme = ThemeData(
        primaryColor: HexColor("#2D6A3F"),
        accentColor: HexColor("#4487c7"),
);