import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'save_views.dart';
import 'visit_view.dart';
import '../salesforce_connection/getdata_salesforce.dart';

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => new RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  var _suggestions;
  Map<String, dynamic> jsonResponse;
  List<Map<String, dynamic>> _saved = new List<Map<String, dynamic>>();
  final _biggerFont = const TextStyle(fontSize: 18.0);
  SalesforceConnection connect = new SalesforceConnection(
      "ancamo@bemen3.cat",
      "flutter2019",
      "QlfbqFU6QnusHqihjp8wEah4S",
      "3MVG9Ve.2wqUVx_ZeigE1gnmacwP8i6Sshmn3vXP9w5cAC9s6ohmmzdDSg0tkrjZ2MbPi0GWJTcQbyFDuvl6w",
      "01ECB18F3B5656187C66F06CD6D1E9BCDC34D2B255A94BE195BC3E75630F7C8A");
  //  SalesforceConnection connect = new SalesforceConnection(
  //       "miguel.tavarez@badob.com",
  //       "Birchman.2019",
  //       "1w1Id4cUMxpoXVusLbZNwooG",
  //       "3MVG9HxRZv05HarQ5An2NKjdMjE1eBEfjBj9WSC4YvrYDFa7bFVcJ.fjkRrqaABVy.i8namQdEr9iPY3ZpcFq",
  //       "3F8EE6CE2525CA9EA0040C21A513091C6CEB53948FC3BAC51937873A87823F23");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.list),
              onPressed: () =>
                  changePage(SaveView(_suggestions, _saved, _biggerFont))),
        ],
      ),
      body: loadPage(),
    );
  }

  void changePage(newPage) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => newPage));
  }

  FutureBuilder loadPage() {
    return FutureBuilder(
      future: connect.retrieveToken("access"),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Text('loading...');
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              // print(snapshot.data);
              // connect.accesToken=snapshot.data;
              // print(connect.accesToken);
              // print(connect.instanceUrl);
              jsonResponse = snapshot.data;
            _suggestions = jsonResponse['records'];
            //print(jsonResponse);
            //print(_suggestions);
            // print(jsonResponse.length);
            return _buildSuggestions();
        }
      },
    );
  }

  Widget _buildSuggestions({bool saveList = false}) {
    // print(jsonResponse);
    // print(jsonResponse['records'][0]['Name']);
    // print(jsonResponse['records'][0]['attributes']);
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, i) {
          Widget returnList;
          if (i.isOdd) return Divider(); /*2*/
          final index = i ~/ 2; /*3*/
          // print(jsonResponse);
          // print(jsonResponse.length);
          // if (index <= _suggestions.length) {
          // if (index == 0 && _suggestions.length < 10) {
          //   _suggestions.addAll(generateWordPairs().take(10)); /*4*/
          // }
          if (saveList) {
            // if (index < _saved.length) {
            //   returnList = _buildRow(_saved[index]);
            // }
          } else {
            if (index < _suggestions.length) {
              returnList = _buildRow(_suggestions[index]);
            }
          }
          return returnList;
        });
  }

  Widget _buildRow(pair) {
    //print(_saved.containsValue(pair['Name']));
    bool alreadySaved = false;
    print(_saved);
    _saved.forEach((value) {
      if (value.containsValue(pair['Name'])) {
        alreadySaved = true;
      }
    });
    // print(pair);
    return ListTile(
      title: Text(
        pair['Name'],
        style: _biggerFont,
      ),
      trailing: IconButton(
        icon: new Icon(alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : null),
        onPressed: () {
          setState(() {
            if (alreadySaved) {
              // _saved.removeAt(pair['Name']);
              _saved.forEach((value) {
                if (value.containsValue(pair['Name'])) {
                  alreadySaved = true;
                }
              });
            } else {
              _saved.add(pair);
            }
          });
        },
      ),
      onTap: () => changePage(ViewLisit(pair)),
    );
  }
}
