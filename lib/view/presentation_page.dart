import 'package:flutter/material.dart';

class PresentationPage extends StatefulWidget {
  // final saveRows;
  // PresentationPage(this.saveRows);
  static final List<String> _listViewData = [
    "Inducesmile.com",
    "Flutter Dev",
    "Android Dev",
  ];
  @override
  PresentationPageState createState() => new PresentationPageState();
}

class PresentationPageState extends State<PresentationPage> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      Text(
        'Web',
        style: optionStyle,
      ),
      Text(
        'Video',
        style: optionStyle,
      ),
      Text(
        'Img',
        style: optionStyle,
      ),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text('Presentación'),
      ),
      drawer: drawerBar(),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
    );
  }

  Drawer drawerBar() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          _createDrawerItem(
            icon: Icons.contacts,
            text: 'Web',
            index: 0,
          ),
          Divider(),
          _createDrawerItem(
            icon: Icons.event,
            text: 'Video',
            index: 1,
          ),
          Divider(),
          _createDrawerItem(
            icon: Icons.note,
            text: 'Img',
            index: 2,
          ),
          Divider(),
          ListTile(
            title: Text('Terminar presentación'),
            onTap: () {
              // Navigator.of(context, rootNavigator: false).pop(true);
              Navigator.pop(context);
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/images/drawer_backgroung.jpg'))),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Flutter Step-by-Step",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }

  Widget _createDrawerItem({IconData icon, String text, int index}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: () => _onItemTapped(index),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
