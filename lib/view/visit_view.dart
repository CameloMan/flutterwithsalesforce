import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:superellipse_shape/superellipse_shape.dart';

import 'presentation_page.dart';

class ViewLisit extends StatefulWidget {
  final saveRows;
  ViewLisit(this.saveRows);
  @override
  ViewLisitState createState() => new ViewLisitState(saveRows);
}

class ViewLisitState extends State<ViewLisit> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  final saveRows;
  ViewLisitState(this.saveRows);
  @override
  Widget build(BuildContext context) {
    Position visitLocation =
        new Position(latitude: 41.374814, longitude: 2.149137);
    Completer<GoogleMapController> _controller = Completer();
    List<Widget> _widgetOptions = <Widget>[
      locationPermition(visitLocation, _controller),
      Text(
        'Presentación',
        style: optionStyle,
      ),
      Text(
        'Fin',
        style: optionStyle,
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
      ),
      // body: futureBuilder,
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: navigationBar(),
    );
  }

  BottomNavigationBar navigationBar() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Inicio'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business),
          title: Text('Presentación'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          title: Text('Fin'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.amber[800],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    if (index == 2) {
      alert();
    } else if (index == 1) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => PresentationPage()));
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  FutureBuilder locationPermition(visitLocation, _controller) {
    return FutureBuilder(
      future: _getData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Text('loading...');
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return pageView(snapshot.data, visitLocation, _controller);
        }
      },
    );
  }

  static Future<Position> _getData() async {
    Position positionUser = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return positionUser;
  }

  CustomScrollView pageView(positionUser, visitLocation, _controller) {
    return CustomScrollView(
      slivers: [
        SliverList(
          delegate: SliverChildListDelegate([
            ListTile(title: Text('First item')),
            ListTile(title: Text('Second item')),
            ListTile(title: Text('Third item')),
            ListTile(
                title: Text(positionUser.latitude.toString() +
                    " " +
                    positionUser.longitude.toString())),
            Stack(
              children: <Widget>[
                _googleMap(positionUser, visitLocation, _controller, context),
              ],
            ),
          ]),
        ),
      ],
    );
  }

  void alert() {
    double rating = 3;
    Alert(
      context: context,
      style: alertStyle(),
      title: "Califica esa visita",
      content: SmoothStarRating(
          allowHalfRating: false,
          onRatingChanged: (v) {
            setState(() {
              rating = v;
            });
          },
          starCount: 5,
          rating: rating,
          size: 40.0,
          color: Colors.green,
          borderColor: Colors.green,
          spacing: 0.0),
      buttons: [
        DialogButton(
          child: Text(
            "cerrar",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop(true);
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  Widget _googleMap(Position position, Position visitLocation,
          Completer mapController, context) =>
      Container(
        margin: const EdgeInsets.all(10.0),
        width: MediaQuery.of(context).size.width,
        height: 300,
        child: GoogleMap(
            mapType: MapType.normal,
            compassEnabled: true,
            scrollGesturesEnabled: true,
            zoomGesturesEnabled: true,
            initialCameraPosition: CameraPosition(
                target: LatLng(position.latitude, position.longitude),
                zoom: 12),
            onMapCreated: (GoogleMapController controller) {
              mapController.complete(controller);
            },
            markers: {markers(visitLocation), markers(position)}),
        // polylines: {}
      );
}

markers(Position position) {
  Marker placeMarker = Marker(
      markerId: MarkerId('Destiny'),
      position: LatLng(position.latitude, position.longitude),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
  return placeMarker;
}

AlertStyle alertStyle() {
  return AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
    alertBorder: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(0.0),
      side: BorderSide(
        color: Colors.grey,
      ),
    ),
    titleStyle: TextStyle(
      color: Colors.red,
    ),
  );
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
