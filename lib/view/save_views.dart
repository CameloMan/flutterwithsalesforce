import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class SaveView extends StatefulWidget {
  final _suggestions;
  final _saved;
  final _biggerFont;
  SaveView(this._suggestions, this._saved, this._biggerFont);
  @override
  SaveViewState createState() =>
      new SaveViewState(_suggestions, _saved, _biggerFont);
}

class SaveViewState extends State<SaveView> {
  final _suggestions;
  final _saved;
  final _biggerFont;
  SaveViewState(this._suggestions, this._saved, this._biggerFont);
  // final ListView saveRows;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildSuggestions({bool saveList = true}) {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, i) {
          Widget returnList;
          if (i.isOdd) return Divider(); /*2*/
          final index = i ~/ 2; /*3*/
          // if (index <= _suggestions.length) {
          if (index == 0 && _suggestions.length < 10) {
            _suggestions.addAll(generateWordPairs().take(10)); /*4*/
          }
          if (saveList) {
            if (index < _saved.length) {
              returnList = _buildRow(_saved[index]);
            }
          } else {
            if (index < _suggestions.length) {
              returnList = _buildRow(_suggestions[index]);
            }
          }
          return returnList;
        });
  }

  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(pair);
          } else {
            _saved.add(pair);
          }
        });
      },
    );
  }
}
