import 'package:flutter/material.dart';
import 'Visit_List.dart';

class _HomePage extends State<HomePage> {
  _HomePage();
  @override
  Widget build(BuildContext context) {
   
    return MaterialApp(
        title: 'Flutter layout demo',
        home: Scaffold(
          body: RandomWords(),
        ));
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}
