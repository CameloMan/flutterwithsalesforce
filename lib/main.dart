import 'package:flutter/material.dart';
import 'view/login_page.dart';
import 'view/home_page.dart';

void main() => runApp(ForceCLMApp());

class _ForceCLMApp extends State<ForceCLMApp> {
  bool _isLoggedIn = true;
  @override
  Widget build(BuildContext context) {
    var page;

    if (_isLoggedIn) {
      page = HomePage();
    } else {
      page = LoginPage();
    }
    return MaterialApp(
        title: 'Flutter layout demo',
        initialRoute: '/',
        routes: {
          '/homepage': (context) => HomePage(),
          '/loginpage': (context) => LoginPage(),
        },
        home: Scaffold(body: page));
  }
}

class ForceCLMApp extends StatefulWidget {
  // This widget is the root of your application.
  _ForceCLMApp createState() => _ForceCLMApp();
}
