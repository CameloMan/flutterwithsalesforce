import 'dart:convert' as convert;
import 'package:http/http.dart';
import 'package:flutter/material.dart';

class SalesforceConnection {
  var params;
  var apiVersion;
  var baseUrl;
  var accesToken, instanceUrl;
  SalesforceConnection(username, password, token, consumerKey, consumerSecret,
      {this.baseUrl = "https://login.salesforce.com",
      this.apiVersion = "v46.0"}) {
    this.params = {
      "grant_type": "password",
      "client_id": consumerKey,
      "client_secret": consumerSecret,
      "username": username,
      "password": password + token
    };
    // this.accesToken = retrieveToken("access");
    // this.instanceUrl = retrieveToken("instance");
    // sfApiCall();
    // print(accesToken);
  }

  retrieveToken(accesInstance) async {
    Response req = await post(baseUrl + "/services/oauth2/token", body: params);
   // print(req.body);
    Map<String, dynamic> jsonResponse = convert.jsonDecode(req.body);
    // var returnCredential =(accesInstance == "access") ? jsonResponse['access_token']:jsonResponse["instance_url"];
    this.accesToken = jsonResponse['access_token'];
    this.instanceUrl = jsonResponse["instance_url"];
    return sfApiCall();
    // return returnCredential;
  }

  sfApiCall() async {
    var headers = {
      'Content-type': 'application/json',
      'Accept-Encoding': 'gzip',
      'Authorization': 'Bearer $accesToken',
    };
    var url = instanceUrl.toString() +
        "/services/data/v46.0/query/?q=SELECT+Name+from+Account+WHERE+Name+LIKE+'Test%'";
    Response req = await get(url, headers: headers);
    Map<String, dynamic> jsonResponse = convert.jsonDecode(req.body);
    // print(req.body);
    // print(jsonResponse);
    // print(jsonResponse['records'][0]['Name']);
    return jsonResponse;
  }
}

getDataSalesforce() async {
  // This example uses the Google Books API to search for books about http.
  // https://developers.google.com/books/docs/overview
  var url = "https://www.googleapis.com/books/v1/volumes?q={http}";

  // Await the http get response, then decode the json-formatted responce.
  var response = await get(url);
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    var itemCount = jsonResponse['totalItems'];
    print("Number of books about http: $itemCount.");
  } else {
    print("Request failed with status: ${response.statusCode}.");
  }
}
